
// ===============================================================================================================
// -*- C++ -*-
//
// Common.hpp - Common includes and definition for the application.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef RT_COMMON_HPP
#define RT_COMMON_HPP

#ifdef _MSC_VER
// Disable some stupid warnings from Visual Studio
#pragma warning (disable : 4996)
#pragma warning (disable : 4985)
#endif // _MSC_VER

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>      // For clock(), clock_t
#include <xmmintrin.h> // SSE intrinsics

// Macros & Definitions:

#ifdef _MSC_VER
#define SIMD_INLINE __forceinline
#define SIMD_ALIGN(x) __declspec(align(16)) x
#else
// This is GCC compatible
#define SIMD_INLINE inline
#define SIMD_ALIGN(x) x __attribute__ ((aligned (16)))
#endif // _MSC_VER

/// Packs 3 color bytes inside an integer. The last channel is set to 255.
#define PackRGB(r, g, b) (static_cast<unsigned int>(((0xff << 24) | (((b) & 0xff) << 16) | (((g) & 0xff) << 8) | ((r) & 0xff))))

/// Packs 4 color bytes inside an integer.
#define PackRGBA(r, g, b, a) (static_cast<unsigned int>((((a) & 0xff) << 24) | (((b) & 0xff) << 16) | (((g) & 0xff) << 8) | ((r) & 0xff)))

/// Fast byte color to normalized [0,1] float color.
#define Byte2Float(x) (static_cast<float>((x) * (1.0f / 255.0f)))

/// Fast normalized float color to [0,255] byte color.
#define Float2Byte(x) (static_cast<unsigned char>((x) * 255.0f))

namespace SimdRT
{

///
/// SIMD 16-byte aligned vector.
/// [x,y,z,w] or [r,g,b,a] component access.
///
SIMD_ALIGN(class) Vector
{
public:

	union
	{
		struct { float x, y, z, w; };
		struct { float r, g, b, a; };
		__m128 v4f;
	};

	// Construction:

	SIMD_INLINE Vector() { /* Uninitialized */ }
	SIMD_INLINE Vector(const __m128 & v) { v4f = v; }
	SIMD_INLINE Vector(const Vector & rhs) { v4f = rhs.v4f; }
	SIMD_INLINE Vector(float X, float Y, float Z, float W = 0.0f) : x(X), y(Y), z(Z), w(W) { }

	// Operators:

	SIMD_INLINE Vector & operator = (const Vector & rhs) { v4f = rhs.v4f; return *this; }

	SIMD_INLINE Vector & operator += (const Vector & rhs) { v4f = _mm_add_ps(v4f, rhs.v4f); return *this; }
	SIMD_INLINE Vector & operator -= (const Vector & rhs) { v4f = _mm_sub_ps(v4f, rhs.v4f); return *this; }

	SIMD_INLINE Vector & operator *= (float f) { v4f = _mm_mul_ps(v4f, _mm_set1_ps(f)); return *this; }
	SIMD_INLINE Vector & operator *= (const Vector & rhs) { v4f = _mm_mul_ps(v4f, rhs.v4f); return *this; }

	SIMD_INLINE Vector operator + (const Vector & rhs) const { return Vector(_mm_add_ps(v4f, rhs.v4f)); }
	SIMD_INLINE Vector operator - (const Vector & rhs) const { return Vector(_mm_sub_ps(v4f, rhs.v4f)); }

	SIMD_INLINE Vector operator * (float f) const { return Vector(_mm_mul_ps(v4f, _mm_set1_ps(f))); }
	SIMD_INLINE Vector operator * (const Vector & rhs) const { return Vector(_mm_mul_ps(v4f, rhs.v4f)); }

	SIMD_INLINE Vector operator - () const { return Vector(-x, -y, -z, -w); }

	// Public Interface:

	SIMD_INLINE float Dot(const Vector & rhs) const
	{
		float res; /* Dot product of vec3(x,y,z); w ignored */
		__m128 __mm_dot3_tmp = _mm_mul_ps(v4f, rhs.v4f);
		__m128 __mm_dot3_res = _mm_add_ps(_mm_shuffle_ps(__mm_dot3_tmp, __mm_dot3_tmp, _MM_SHUFFLE(0, 0, 0, 0)),
							   _mm_add_ps(_mm_shuffle_ps(__mm_dot3_tmp, __mm_dot3_tmp, _MM_SHUFFLE(1, 1, 1, 1)),
							   _mm_shuffle_ps(__mm_dot3_tmp, __mm_dot3_tmp, _MM_SHUFFLE(2, 2, 2, 2))));
		_mm_store_ss(&res, __mm_dot3_res);
		return res;
	}

	SIMD_INLINE float Length() const
	{
		float res; /* Length of vec3(x,y,z); w ignored */
		__m128 __mm_len3_tmp = _mm_mul_ps(v4f, v4f);
		__m128 __mm_len3_res = _mm_add_ps(_mm_shuffle_ps(__mm_len3_tmp, __mm_len3_tmp, _MM_SHUFFLE(0, 0, 0, 0)),
							   _mm_add_ps(_mm_shuffle_ps(__mm_len3_tmp, __mm_len3_tmp, _MM_SHUFFLE(1, 1, 1, 1)),
							   _mm_shuffle_ps(__mm_len3_tmp, __mm_len3_tmp, _MM_SHUFFLE(2, 2, 2, 2))));
		_mm_store_ss(&res, _mm_sqrt_ps(__mm_len3_res));
		return res;
	}

	SIMD_INLINE float LengthSquared() const
	{
		float res; /* Squared length of vec3(x,y,z); w ignored */
		__m128 __mm_len3_tmp = _mm_mul_ps(v4f, v4f);
		__m128 __mm_len3_res = _mm_add_ps(_mm_shuffle_ps(__mm_len3_tmp, __mm_len3_tmp, _MM_SHUFFLE(0, 0, 0, 0)),
							   _mm_add_ps(_mm_shuffle_ps(__mm_len3_tmp, __mm_len3_tmp, _MM_SHUFFLE(1, 1, 1, 1)),
							   _mm_shuffle_ps(__mm_len3_tmp, __mm_len3_tmp, _MM_SHUFFLE(2, 2, 2, 2))));
		_mm_store_ss(&res, __mm_len3_res);
		return res;
	}

	SIMD_INLINE Vector & Normalize()
	{
		/* Normalize the vec3(x,y,z); w ignored */
		__m128 __mm_norm3_tmp0 = _mm_mul_ps(v4f, v4f);
		__m128 __mm_norm3_tmp1 = _mm_add_ps(_mm_shuffle_ps(__mm_norm3_tmp0, __mm_norm3_tmp0, _MM_SHUFFLE(0, 0, 0, 0)),
								 _mm_add_ps(_mm_shuffle_ps(__mm_norm3_tmp0, __mm_norm3_tmp0, _MM_SHUFFLE(1, 1, 1, 1)),
								 _mm_shuffle_ps(__mm_norm3_tmp0, __mm_norm3_tmp0, _MM_SHUFFLE(2, 2, 2, 2))));

		/* Calculate Newton-Raphson Reciprocal Square Root with formula:
		0.5 * rsqrt(x) * (3 - x * rsqrt(x)^2) */

		static const __m128 v0pt5 = { 0.5f, 0.5f, 0.5f, 0.5f };
		static const __m128 v3pt0 = { 3.0f, 3.0f, 3.0f, 3.0f };

		__m128 t = _mm_rsqrt_ps(__mm_norm3_tmp1);
		__m128 __mm_norm3_tmp2 = _mm_mul_ps(_mm_mul_ps(v0pt5, t), _mm_sub_ps(v3pt0, _mm_mul_ps(_mm_mul_ps(__mm_norm3_tmp1, t), t)));

		v4f = _mm_mul_ps(v4f, __mm_norm3_tmp2);
		return *this;
	}

	SIMD_INLINE Vector Cross(const Vector & rhs) const
	{
		Vector res; /* Cross product of vec3(x,y,z) this & rhs; w ignored for both */
		__m128 __mm_c3_tmp0 = _mm_shuffle_ps(v4f,     v4f,     _MM_SHUFFLE(3, 0, 2, 1));
		__m128 __mm_c3_tmp1 = _mm_shuffle_ps(rhs.v4f, rhs.v4f, _MM_SHUFFLE(3, 1, 0, 2));
		__m128 __mm_c3_tmp2 = _mm_shuffle_ps(v4f,     v4f,     _MM_SHUFFLE(3, 1, 0, 2));
		__m128 __mm_c3_tmp3 = _mm_shuffle_ps(rhs.v4f, rhs.v4f, _MM_SHUFFLE(3, 0, 2, 1));

		res.v4f = _mm_sub_ps(_mm_mul_ps(__mm_c3_tmp0, __mm_c3_tmp1), _mm_mul_ps(__mm_c3_tmp2, __mm_c3_tmp3));
		return res;
	}

	SIMD_INLINE float Distance(const Vector & rhs) const
	{
		float res; /* Length of vec3(this - rhs) */
		__m128 __mm_diff = _mm_sub_ps(v4f, rhs.v4f);
		__m128 __mm_len3_tmp = _mm_mul_ps(__mm_diff, __mm_diff);
		__m128 __mm_len3_res = _mm_add_ps(_mm_shuffle_ps(__mm_len3_tmp, __mm_len3_tmp, _MM_SHUFFLE(0, 0, 0, 0)),
							   _mm_add_ps(_mm_shuffle_ps(__mm_len3_tmp, __mm_len3_tmp, _MM_SHUFFLE(1, 1, 1, 1)),
							   _mm_shuffle_ps(__mm_len3_tmp, __mm_len3_tmp, _MM_SHUFFLE(2, 2, 2, 2))));
		_mm_store_ss(&res, _mm_sqrt_ps(__mm_len3_res));
		return res;
	}
};

// ===============================================================================================================
// Aligned memory allocations

SIMD_INLINE void * AlignedMalloc(size_t numBytes)
{
	void * memory;

#ifdef _MSC_VER
	// Visual Studio - Windows
	memory = _aligned_malloc(numBytes, 16);
#elif defined (__APPLE__)
	// According to Apple, malloc() return memory
	// suitably aligned for SSE and AltiVec operations.
	// I have no reason to think otherwise :P
	memory = malloc(numBytes);
#else
	// GCC - Linux
	memory = memalign(16, numBytes);
#endif

	if (!memory)
	{
		fprintf(stderr, "FATAL ERROR! Out-Of-Memory");
		exit(EXIT_FAILURE);
	}

	return memory;
}

SIMD_INLINE void AlignedFree(void * memory)
{
	if (memory != 0)
	{
#ifdef _MSC_VER
		_aligned_free(memory);
#else
		free(memory);
#endif
	}
}

// ===============================================================================================================
// Timing

SIMD_INLINE float GetElapsedMilliseconds()
{
	static const clock_t baseTime = clock();
	return static_cast<float>(clock() - baseTime);
}

SIMD_INLINE float GetElapsedSeconds()
{
	return GetElapsedMilliseconds() * 0.001f;
}

} // namespace SimdRT {}

#endif // RT_COMMON_HPP
