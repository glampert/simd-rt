
// ===============================================================================================================
// -*- C++ -*-
//
// Raytracer.cpp - Definition of the Raytracer renderer.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#include "Scene.hpp"
#include "Raytracer.hpp"
#include "Thread.hpp"

#include <math.h>
#include <string.h> // For memset / memcpy

namespace SimdRT
{

// Static member data:
void * Raytracer::pixelBuf = 0;
int Raytracer::width       = 0;
int Raytracer::height      = 0;
PixelFormat Raytracer::pixelFmt;

// RT Implementation:

bool Raytracer::Initialize(int sceneWidth, int sceneHeight, PixelFormat pxFmt)
{
	assert((sceneWidth > 0) && (sceneHeight > 0));

	width  = sceneWidth;
	height = sceneHeight;
	pixelFmt = pxFmt;

	size_t pixSize;

	switch (pixelFmt)
	{
	case PF_RGBA_UINT8:
		pixSize = sizeof(unsigned char) * 4;
		break;

	case PF_RGBA_FLOAT32:
		pixSize = sizeof(float) * 4;
		break;
	}

	// Allocate the pixel buffer
	pixelBuf = AlignedMalloc(width * height * pixSize);
	memset(pixelBuf, 0, (width * height * pixSize));
	return true;
}

#if defined (SIMDRT_MULTITHREAD)

void Raytracer::RendererThread(void * parameter)
{
	const RT_ThreadData * rt = reinterpret_cast<const RT_ThreadData *>(parameter);

	float dist; // Not in use...
	float sX1, sY1 = rt->sY;

	for (int y = rt->y; y < rt->height; ++y)
	{
		sX1 = rt->sX;

		// Render pixels for current line
		for (int x = rt->x; x < rt->width; ++x)
		{
			// Reset vars:
			Vector color(0.0f, 0.0f, 0.0f, 1.0f);

			Vector dir(sX1, sY1, 0.0f);
			dir -= rt->cameraPos;
			dir.Normalize();

			// Fire primary ray
			Ray r(rt->cameraPos, dir);

			// Trace the ray and calculate color
			TraceRay(rt->sceneBuffer, r, color, dist);

			// Write pixel to color buffer;
			PutPixel(&color, x, y);

			sX1 += rt->dX;
		}

		sY1 += rt->dY;
	}
}

#endif // SIMDRT_MULTITHREAD

void Raytracer::RenderScene(const Scene * theScene, const Vector & cameraPos, const Vector & screenPlane)
{
	assert(theScene != 0);

	// Calculate deltas for interpolation
	const float dX = (screenPlane.y - screenPlane.x) / width;
	const float dY = (screenPlane.w - screenPlane.z) / height;

#if defined (SIMDRT_MULTITHREAD)

	// Running parallel on 4 threads:

	// Right-shift is way cooler :-P
	const int halfWidth  = width  >> 1;
	const int halfHeight = height >> 1;

	// 4 worker threads

	// Thread 0
	RT_ThreadData rtThread0 = {0, halfWidth, 0, halfHeight, screenPlane.x, (dY + 3.0f), dX, dY, theScene, cameraPos};
	Thread thread0(Raytracer::RendererThread, &rtThread0);

	// Thread 1
	RT_ThreadData rtThread1 = {halfWidth, width, halfHeight, height,
	(screenPlane.x + dX * static_cast<float>(halfWidth)), ((dY + 3.0f) + dY * static_cast<float>(halfHeight)), dX, dY, theScene, cameraPos};
	Thread thread1(Raytracer::RendererThread, &rtThread1);

	// Thread 2
	RT_ThreadData rtThread2 = {0, halfWidth, halfHeight, height,
	screenPlane.x, ((dY + 3.0f) + dY * static_cast<float>(halfHeight)), dX, dY, theScene, cameraPos};
	Thread thread2(Raytracer::RendererThread, &rtThread2);

	// Thread 3
	RT_ThreadData rtThread3 = {halfWidth, width, 0, halfHeight,
	(screenPlane.x + dX * static_cast<float>(halfWidth)), (dY + 3.0f), dX, dY, theScene, cameraPos};
	Thread thread3(Raytracer::RendererThread, &rtThread3);

	// Wait for all thread to finish and then return to the caller
	thread0.join();
	thread1.join();
	thread2.join();
	thread3.join();

#else

	// Running in 1 thread only:

	float dist;
	float sY1 = dY + 3.0f;

	for (int y = 0; y < height; ++y)
	{
		float sX1 = screenPlane.x;

		// Render pixels for current line
		for (int x = 0; x < width; ++x)
		{
			// Reset vars:
			Vector color(0.0f, 0.0f, 0.0f, 1.0f);

			Vector dir(sX1, sY1, 0.0f);
			dir -= cameraPos;
			dir.Normalize();

			// Fire primary ray
			Ray r(cameraPos, dir);

			// Trace the ray and calculate color
			TraceRay(theScene, r, color, dist);

			// Write pixel to color buffer;
			PutPixel(&color, x, y);

			sX1 += dX;
		}

		sY1 += dY;
	}

#endif // SIMDRT_MULTITHREAD
}

const void * Raytracer::GetOutputScene()
{
	return pixelBuf;
}

void Raytracer::GetOutputDimensions(int & w, int & h)
{
	w = width;
	h = height;
}

PixelFormat Raytracer::GetOutputPixelFormat()
{
	return pixelFmt;
}

void Raytracer::Finalize()
{
	// Release allocated memory:
	AlignedFree(pixelBuf);
	pixelBuf = 0;
}

const Primitive * Raytracer::TraceRay(const Scene * theScene, const Ray & ray, Vector & color, float & dist)
{
	dist = 1000000.0f;
	const Primitive * prim = 0;
	const int numPrimitives = theScene->GetNumPrimitives();

	// Find the nearest intersection
	for (int s = 0; s < numPrimitives; ++s)
	{
		const Primitive * pr = theScene->GetPrimitive(s);
		if (pr->Intersect(ray, dist))
		{
			prim = pr;
		}
	}

	// No hit, terminate ray
	if (!prim)
	{
		return 0;
	}

	// Handle intersection
	if (prim->IsLight())
	{
		// We hit a light, stop tracing
		color = Vector(1.0f, 1.0f, 1.0f, 1.0f);
	}
	else
	{
		// Determine color at point of intersection
		Vector intersectionPt(ray.origin + ray.direction * dist);

		// Trace lights
		for (int l = 0; l < numPrimitives; ++l)
		{
			const Primitive * p = theScene->GetPrimitive(l);

			if (p->IsLight())
			{
				float shade = 1.0f;
				const Primitive * light = p;
				const Material & mat = prim->GetMaterial();

				Vector Lvec(reinterpret_cast<const Sphere *>(light)->center - intersectionPt);

				float tdist = Lvec.Length();
				Lvec *= (1.0f / tdist);

				Ray r(intersectionPt + Lvec * 0.0001f, Lvec);

				for (int s = 0; s < numPrimitives; ++s)
				{
					const Primitive * pr = theScene->GetPrimitive(s);
					if ((pr != light) && (pr->Intersect(r, tdist)))
					{
						shade = 0.0f;
						break;
					}
				}

				Vector L(reinterpret_cast<const Sphere *>(light)->center - intersectionPt);
				L.Normalize();

				Vector N(prim->GetNormal(intersectionPt));

				// Calculate diffuse shading
				if (mat.diffuse > 0.0f)
				{
					const float dot = N.Dot(L);

					if (dot > 0.0f)
					{
						const float diff = (dot * mat.diffuse * shade);
						// Add diffuse component to ray color
						color += (mat.color * light->GetMaterial().color * diff);
					}
				}

				// Calculate specular component
				if (mat.specular > 0.0f)
				{
					Vector V(ray.direction);
					Vector R(L - (N * (2.0f * L.Dot(N))));

					const float dot = V.Dot(R);

					if (dot > 0.0f)
					{
						const float spec = powf(dot, 20.0f) * mat.specular * shade;
						// Add specular component to ray color
						color += light->GetMaterial().color * spec;
					}
				}
			}
		}
	}

	// Return pointer to primitive hit by primary ray
	return prim;
}

void Raytracer::PutPixel(const void * pixel, int x, int y)
{
	const Vector & color = *reinterpret_cast<const Vector *>(pixel);
	switch (pixelFmt)
	{
	case PF_RGBA_UINT8:
		{
			// Need color convertion:
			unsigned char r = Float2Byte(color.r);
			unsigned char g = Float2Byte(color.g);
			unsigned char b = Float2Byte(color.b);
			unsigned char a = Float2Byte(color.a);
			reinterpret_cast<unsigned int *>(pixelBuf)[x + y * width] = PackRGBA(r, g, b, a);
			break;
		}
	case PF_RGBA_FLOAT32:
		{
			// Output is the same as the internal format:
			reinterpret_cast<Vector *>(pixelBuf)[x + y * width] = color;
			break;
		}
	}
}

} // namespace SimdRT
