
## SIMD RT - A tiny Raytracer renderer with a few SIMD optimizations

A minimal multi-threaded Raytracer renderer, with some SIMD/SSE optimizations.
University class assignment.

----

![SIMD-RT](https://bytebucket.org/glampert/simd-rt/raw/d54594457393e17ff6971a6ebb35ad72df7b13d9/bin/Scene.png "SIMD-RT")

----

This project's source code is released under the [MIT License](http://opensource.org/licenses/MIT).

