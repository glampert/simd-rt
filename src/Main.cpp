
// ===============================================================================================================
// -*- C++ -*-
//
// Main.hpp - Application entry point.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

// Local Includes
#include "Common.hpp"
#include "Raytracer.hpp"
#include "Scene.hpp"

// C/C++ Includes
#include <stdarg.h>

// GLUT/OGL used to display RT result to screen only.
#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut>
#endif

// If this define is set the Raytracer sample program will try to run in real time.
// If your computer is fast enough you may get 7+ FPS.
#define RT_REAL_TIME

// ===============================================================================================================
// Local Data:

// Static data is set to zero by default
static int windowWidth;
static int windowHeight;

// Camera, screen data
static SimdRT::Vector cameraPos, screenPlane;
static const float cameraMoveSpeed = 5.0f * (1.0f / 60.0f);

// Scene to be raytraced
static SimdRT::Scene * theScene;
static const void * screenBuffer;

// ===============================================================================================================
// Code:

bool WritePPM(const char * fileName, const void * pixels, int w, int h)
{
	assert((fileName != 0) && (pixels != 0) && (w > 0) && (h > 0));

	// We can also save the raytraced scene as a .ppm image !

	struct PPMpix
	{
		unsigned char r, g, b, a;
	};

	FILE * file = fopen(fileName, "wt"); // Write image to PPM file.
	if (file != 0)
	{
		const PPMpix * px = reinterpret_cast<const PPMpix *>(pixels);

		fprintf(file, "P3\n%i %i\n%i\n", w, h, 255);

		for (int i = 0; i < (w * h); ++i)
		{
			fprintf(file, "%i %i %i ", px[i].r, px[i].g, px[i].b);
		}

		fclose(file);
		return true;
	}

	return false;
}

GLint glPrintf(GLint x, GLint y, const char * format, ...)
{
	char buffer[2048];
	va_list vaList;
	GLint nCount, i;

	va_start(vaList, format);
	nCount = vsnprintf(buffer, sizeof(buffer), format, vaList);
	va_end(vaList);

	glRasterPos2i(x, y);

	for (i = 0; i < nCount; ++i)
	{
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, buffer[i]);
	}

	return nCount;
}

float CalcFPS()
{
	static float ret = 0.0f;
	static float fps = 0.0f;
	static float lastTime = 0.0f;

	// clock() was working for windows,
	// but in the MAC it just doesn't do it.
	//float currentTime = static_cast<float>((clock() * 0.001f));

	float currentTime = static_cast<float>((glutGet(GLUT_ELAPSED_TIME) * 0.001f));
	fps += 1.0f;

	if ((currentTime - lastTime) > 1.0f)
    {
		lastTime = currentTime;
		ret = fps;
		fps = 0.0f;
	}

	return ret;
}

void DisplayCallback()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glLoadIdentity();

#if defined (RT_REAL_TIME)

	// Raytrace the scene
	SimdRT::Raytracer::RenderScene(theScene, cameraPos, screenPlane);

	// Get resulting pixel buffer
	screenBuffer = SimdRT::Raytracer::GetOutputScene();

#endif // RT_REAL_TIME

	glRasterPos3f(0.0f, 0.0f, 0.0f);

	// We'll have to flip the image before drawing, this code will do the trick:
	const GLubyte bitmap[1] = {0};
	glBitmap(0, 0, 0.0f, 0.0f, 0.0f, static_cast<float>(windowHeight), bitmap);

	glPixelZoom(1.0f, -1.0f);

	glDrawPixels(windowWidth, windowHeight, GL_RGBA, GL_UNSIGNED_BYTE, screenBuffer);

	glPixelZoom(1.0f, 1.0f);

#if defined (RT_REAL_TIME)

	// In real-time mode, display the frame rate:
	glPrintf(10, windowHeight - 30, "FPS: %.2f", CalcFPS());

#endif // RT_REAL_TIME

	glutSwapBuffers();
}

void ReshapeWindowCallback(int w, int h)
{
	// Block window resize:
	glutReshapeWindow(windowWidth, windowHeight);

	// Reset the viewport:
	glViewport(0, 0, windowWidth, windowHeight);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Set a 2D projection:
	glOrtho(0.0, static_cast<double>(windowWidth), 0.0, static_cast<double>(windowHeight), 0.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void KeyboardCallback(unsigned char key, int x, int y)
{
	switch (key)
	{
#if defined (RT_REAL_TIME) // Allow camera movement in real time mode:

	case 'w':
		cameraPos.z -= cameraMoveSpeed;
		break;

	case 's':
		cameraPos.z += cameraMoveSpeed;
		break;

	case 'd':
		cameraPos.x -= cameraMoveSpeed;
		break;

	case 'a':
		cameraPos.x += cameraMoveSpeed;
		break;

	case 'q':
		cameraPos.y -= cameraMoveSpeed;
		break;

	case 'e':
		cameraPos.y += cameraMoveSpeed;
		break;

#endif // RT_REAL_TIME

	case 27: // Quit if ESCAPE pressed
		exit(0);

	default:
		break;
	}
}

void Terminate()
{
	// Cleanup before exit
	delete theScene;
	SimdRT::Raytracer::Finalize();
}

int main(int argc, char * argv[])
{
	printf("SIMD Raytracer started...\n");

	// Open the raytracer config file
	SimdRT::INIFile sceneCfg;

	// Must have the CFG file !
	if ((argc < 2) || !sceneCfg.Read(argv[1]))
	{
		printf("ERROR! Cannot open CFG file, quitting...\n");
		return 0;
	}

	const SimdRT::INISection * iniSect = sceneCfg.GetSection("Scene CFG");

	iniSect->GetInteger("ViewportW", windowWidth);
	iniSect->GetInteger("ViewportH", windowHeight);

	// Set Up Glut & OpenGL
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA); // 2D Mode
	glutInitWindowSize(windowWidth, windowHeight);
	glutCreateWindow("SIMD Raytracer - By Guilherme R. Lampert");
	atexit(Terminate);

	// Set 2D pojection for OGL
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, static_cast<double>(windowWidth), 0.0, static_cast<double>(windowHeight), 0.0, 1.0);

	// Window
	glutReshapeFunc(ReshapeWindowCallback);

	// Display
	glutDisplayFunc(DisplayCallback);

	// Keyboard
	glutKeyboardFunc(KeyboardCallback);

	// Init the raytracer engine
	SimdRT::Raytracer::Initialize(windowWidth, windowHeight, SimdRT::PF_RGBA_UINT8);

	// Set up the scene
	theScene = new SimdRT::Scene(&sceneCfg);

	// Get some scene config vars
	iniSect->GetVector("CameraPos", cameraPos);
	iniSect->GetVector("ScreenPlane", screenPlane);

#if defined (RT_REAL_TIME)

	printf("Raytracer running in real time !\n");

	// In real time mode always refresh the screen
	glutIdleFunc(DisplayCallback);

#else

	// Non real time, just render the scene once and save result to a PPM file:

	printf("Rendering scene, please wait...\n");

	float start = SimdRT::GetElapsedSeconds();

	// Raytrace the scene
	SimdRT::Raytracer::RenderScene(theScene, cameraPos, screenPlane);

	// Get resulting pixel buffer
	screenBuffer = SimdRT::Raytracer::GetOutputScene();

	float end = SimdRT::GetElapsedSeconds();

	printf("\n*** DONE ***\n");
	printf("Scene rendered in %.2f seconds!\n", (end - start));

	// Save the rendered scene to an image
	if (WritePPM("Scene.ppm", screenBuffer, windowWidth, windowHeight))
	{
		printf("\nOutput saved to \"Scene.ppm\".\n");
	}
	else
	{
		printf("\nUnable to save output image to file!\n");
	}

#endif // RT_REAL_TIME

	// Run main loop and render the scene
	glutMainLoop();
	return 0;
}

// ===============================================================================================================
// Aligned memory new & delete operators
// Custom new and delete operators doesn't need to be globally visible,
// it is enough just to implement 'em somewhere in the project. Cool :)

void * operator new(size_t numBytes) throw (std::bad_alloc)
{
	return (SimdRT::AlignedMalloc(numBytes));
}

void * operator new[](size_t numBytes) throw (std::bad_alloc)
{
	return (SimdRT::AlignedMalloc(numBytes));
}

void operator delete(void * memory) throw ()
{
	SimdRT::AlignedFree(memory);
}

void operator delete[](void * memory) throw ()
{
	SimdRT::AlignedFree(memory);
}
