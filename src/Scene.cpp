
// ===============================================================================================================
// -*- C++ -*-
//
// Scene.cpp - Scene related interfaces and clases.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#include "Scene.hpp"
#include <string.h> // C String library

namespace SimdRT
{

// ===============================================================================================================
// Class Primitive:

Primitive::Primitive(const Material & mat, bool light, const char * name)
{
	this->material.diffuse    = mat.diffuse;
	this->material.reflection = mat.reflection;
	this->material.color      = mat.color;
	this->isLight             = light;

	if (name != 0)
	{
		const size_t n = strlen(name) + 1;
		this->name = new char[n];
		memmove(this->name, name, n);
	}
}

void Primitive::SetName(const char * name)
{
	assert(name != 0);
	delete[] this->name;

	const size_t n = strlen(name) + 1;
	this->name = new char[n];
	memmove(this->name, name, n);
}

Primitive::~Primitive()
{
	delete[] this->name;
}

// ===============================================================================================================
// Class Sphere:

HitType Sphere::Intersect(const Ray & ray, float & dist) const
{
	Vector v(ray.origin - center);
	float b = -v.Dot(ray.direction);
	float det = (b * b) - v.Dot(v) + sqrRadius;
	HitType retval = RAY_MISS;

	if (det > 0.0f)
	{
		// Use faster SSE square root
		_mm_store_ss(&det, _mm_sqrt_ps(_mm_set1_ps(det)));

		float i1 = b - det;
		float i2 = b + det;

		if (i2 > 0)
		{
			if (i1 < 0)
			{
				if (i2 < dist)
				{
					dist = i2;
					retval = RAY_IN_PRIMITIVE;
				}
			}
			else
			{
				if (i1 < dist)
				{
					dist = i1;
					retval = RAY_HIT;
				}
			}
		}
	}

	return retval;
}

Vector Sphere::GetNormal(const Vector & pos) const
{
	return (pos - center) * (1.0f / radius);
}

// ===============================================================================================================
// Class Plane:

HitType Plane::Intersect(const Ray & ray, float & dist) const
{
	float d = data.Dot(ray.direction);
	if (d != 0.0f)
	{
		float dt = -(data.Dot(ray.origin) + data.w) / d;
		if (dt > 0.0f)
		{
			if (dt < dist)
			{
				dist = dt;
				return RAY_HIT;
			}
		}
	}

	return RAY_MISS;
}

Vector Plane::GetNormal(const Vector & pos) const
{
	return data;
}

// ===============================================================================================================
// Class Scene:

Scene::Scene(const INIFile * sceneCfg)
{
	assert(sceneCfg != 0);

	float f;
	Vector vec;
	Material mat;
	int isLight;
	std::string name;
	const INISection * sect;

	const INIFile::SectionMap & sectMap = sceneCfg->GetAllSections();
	INIFile::SectionMap::const_iterator it  = sectMap.begin();
	INIFile::SectionMap::const_iterator end = sectMap.end();

	// Iterate thru all sections and find the primitives
	while (it != end)
	{
		// Get all scene objects:

		if (strstr((*it).first.c_str(), "Plane") != 0) // Look for plane primitives
		{
			sect = (*it).second;

			// Found a plane:
			sect->GetString("Name", name);
			sect->GetVector("Normal", vec);
			sect->GetFloat("D", f);
			sect->GetFloat("Reflection", mat.reflection);
			sect->GetFloat("Diffuse", mat.diffuse);
			sect->GetVector("Color", mat.color);
			sect->GetInteger("IsLight", isLight);

			// Add primitive to scene:
			primitives.push_back(new Plane(vec, f, mat, (isLight == 1), name.c_str()));
		}
		else if (strstr((*it).first.c_str(), "Sphere") != 0) // Look for spheres
		{
			sect = (*it).second;

			// Found a sphere:
			sect->GetString("Name", name);
			sect->GetVector("Center", vec);
			sect->GetFloat("Radius", f);
			sect->GetFloat("Reflection", mat.reflection);
			sect->GetFloat("Diffuse", mat.diffuse);
			sect->GetVector("Color", mat.color);
			sect->GetInteger("IsLight", isLight);

			// Add primitive to scene:
			primitives.push_back(new Sphere(vec, f, mat, (isLight == 1), name.c_str()));
		}

		++it;
	}
}

int Scene::GetNumPrimitives() const
{
	return primitives.size();
}

const Primitive * Scene::GetPrimitive(int idx) const
{
	return primitives[idx];
}

Scene::~Scene()
{
	for (size_t i = 0; i < primitives.size(); ++i)
	{
		delete primitives[i];
	}
}

} // namespace SimdRT
