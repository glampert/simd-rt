
// ===============================================================================================================
// -*- C++ -*-
//
// Thread.hpp - Declaration of a Thread wrapper class.
//
// Copyright (c) 2011 Guilherme R. Lampert
// guilherme.ronaldo.lampert@gmail.com
//
// This code is licenced under the MIT license.
//
// This software is provided "as is" without express or implied
// warranties. You may freely copy and compile this source into
// applications you distribute provided that the copyright text
// above is included in the resulting source code.
//
// ===============================================================================================================

#ifndef RT_THREADS_HPP
#define RT_THREADS_HPP

#ifdef _MSC_VER
#include <process.h>
#else
#include <pthread.h>
#endif

namespace SimdRT
{

typedef void (* ThreadFunc)(void * userData);

///
/// Simple thread wrapper that supports Win32 Threads
/// and POSIX PThreads.
///
class Thread
{
public:

	/// Creates and executes a new thread.
	Thread(ThreadFunc fn, void * ud);

	/// Wait for the thread to finish execution.
	void join();

	~Thread();

private:

	// Disallow copy and assignment
	Thread(const Thread &);
	Thread & operator = (const Thread &);

#ifdef _MSC_VER
	void * threadHandle;
	static unsigned int __stdcall MyThreadProc(void * ctx);
#else
	pthread_t threadHandle;
	static void * MyThreadProc(void * ctx);
#endif

	/// Pointer to user code to be executed.
	ThreadFunc userFunc;

	/// Pointer to user defined data to be passed to the thread.
	void * userData;
};

} // namespace SimdRT

#endif // RT_THREADS_HPP
